package com.parsing.test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class LogParsing {
	private static final String SUCCESS = "200";

	/**
	 * API호출 기록 데이터 추출 작업
	 */
	public void coding () {
		File readfile = new File("D:\\input.log");
		File outfile = new File("D:\\output.log");

		try {
			StringBuffer resultSb = new StringBuffer();
			BufferedReader br = new BufferedReader(new FileReader(readfile));
			String readline = null;
			String[] splitStr = null;
			List<ApiInfo> apiRawList = new ArrayList<ApiInfo>();

			while ((readline = br.readLine()) != null) {
				readline = readline.replaceAll("\\]\\[", ",");
				readline = readline.replaceAll("\\[", "");
				readline = readline.replaceAll("\\]", "");
				splitStr = readline.split(",");

				apiRawList.add(new ApiInfo(splitStr[0], splitStr[1], splitStr[2], splitStr[3]));
			}

			int successCount = 0;
			Map<String, Integer> apikeyMap = new HashMap<String, Integer>(); 
			Map<String, Integer> serviceIdMap = new HashMap<String, Integer>(); 
			Map<String, Integer> browserMap = new HashMap<String, Integer>(); 
			for (int i=0; i<apiRawList.size(); i++) {
				String status = apiRawList.get(i).getStatus();
				String url = apiRawList.get(i).getUrl();
				String browser = apiRawList.get(i).getBrowser();
				if (SUCCESS.equals(status)) {
					parsing (apikeyMap, url, 1);

					if (browserMap.containsKey(browser)) {
						browserMap.put(browser, browserMap.get(browser)+1);
					} else {
						browserMap.put(browser, 1);
					}
					successCount++;
				}
			}
			
			resultSb.append("최다호출 API KEY\n");
			int compValue = 0;
			String maxApiKey = null;
			for (Map.Entry<String, Integer> e : apikeyMap.entrySet()) {
				String key = e.getKey();
				Integer value = e.getValue();
				if (value > compValue) {
					compValue = value;
					maxApiKey = key;
				}
			}
			
			resultSb.append(maxApiKey);
			resultSb.append("\n\n상위 3개의 API ServiceID와 각각의 요청 수\n");
			List<String> maxApiList = new ArrayList<String>(apikeyMap.keySet());
			Collections.sort(maxApiList, new Comparator<String>() {
				@Override
				public int compare(String o1, String o2) {
					return apikeyMap.get(o2).compareTo(apikeyMap.get(o1));
				}
			});
			
			//상위 3개의 API KEY에 해당하는 ServiceID와 각각 요청수 재추출
			for (int i=0; i<maxApiList.size(); i++) {
				if (i < 3) {
					for (int j=0; j<apiRawList.size(); j++) {
						String status = apiRawList.get(i).getStatus();
						String url = apiRawList.get(j).getUrl();
						if (url.contains(maxApiList.get(i))) {
							if (SUCCESS.equals(status)) {
								parsing (serviceIdMap, url, 0);
							}
						}
					}
				}
			}
			
			List<String> serviceIdList = new ArrayList<String>(serviceIdMap.keySet());
			Collections.sort(serviceIdList, new Comparator<String>() {
				@Override
				public int compare(String o1, String o2) {
					return serviceIdMap.get(o2).compareTo(serviceIdMap.get(o1));
				}
			});
			for (int i=0; i<serviceIdList.size(); i++) {
				if (i < 3) {
					resultSb.append(serviceIdList.get(i) + ": " + serviceIdMap.get(serviceIdList.get(i))+"\n");
				}
			}
			
			resultSb.append("\n웹브라우저별 사용 비율\n");
			List<String> browserList = new ArrayList<String>(browserMap.keySet());
			Collections.sort(browserList, new Comparator<String>() {
				@Override
				public int compare(String o1, String o2) {
					return browserMap.get(o2).compareTo(browserMap.get(o1));
				}
			});
			for (String bl : browserList) {
				int percent = (int)((double)browserMap.get(bl)/(double)successCount*100.0);
				resultSb.append(bl + ": " + percent + "%" + "\n");
			}
			br.close();

			BufferedWriter bw = new BufferedWriter(new FileWriter(outfile));
			bw.write(resultSb.toString());
			bw.flush();
			bw.close(); 

			System.out.println(resultSb.toString());
			System.out.println("==================================");
			System.out.println("success count: " + successCount);
			System.out.println("total count: " + apiRawList.size());
			System.out.println("==================================");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Api 정보
	 */
	public class ApiInfo{
		String status;
		String url;
		String browser;
		String calldate;

		ApiInfo(String status, String url, String browser, String calldate) {
			this.status = status;
			this.url = url;
			this.browser = browser;
			this.calldate = calldate;
		}
		public String getStatus() {
			return status;
		}
		public String getUrl() {
			return url;
		}
		public String getBrowser() {
			return browser;
		}
		public String getCalldate() {
			return calldate;
		}
	}

	/**
	 * parsing 공통
	 */
	public void parsing (Map<String, Integer> mapName, String url, int idx) {
		String temp = null, subTemp = null;
		StringTokenizer st = new StringTokenizer(url, "/") ;
		while (st.hasMoreTokens()) {
			temp = st.nextToken();
			if (temp.indexOf("&") >= 0) {
				StringTokenizer subSt = new StringTokenizer(temp, "&");
				while (subSt.hasMoreTokens()) {
					temp = subSt.nextToken();
					if (temp.indexOf("?") >= 0) {
						subTemp = temp.replace("?apikey=", ",");
						String[] subTempArr = subTemp.split(",");
						if (mapName.containsKey(subTempArr[idx])) {
							mapName.put(subTempArr[idx], mapName.get(subTempArr[idx])+1);
						} else {
							mapName.put(subTempArr[idx], 1);
						}
					}
				}
			}
		}
	}

	public static void main(String[] args) {
		LogParsing coding = new LogParsing();
		coding.coding();
	}

}
