##문제해결 방법

~~~
1. 로그파일(input.log)내 상태, URL, 브라우저, 호출일 4개의 필드로 구분해서 API 정보의 List 생성
2. 위에 생성한 List에서 URL내용을 parsing 공통 메소드 호출해 ServiceID, API KEY 추출
3. 브라우저는 List에서 있는 값 이용, ServiceID, API KEY는 URL에서 다시 추출
   추출한 데이터를 각 Map에 key=추출명, value=건수로 루프 돌면서 중복되는 key가 있으면 value에 카운트
4. 각각의 Map을 가져와서 데이터 가공   
    > 최다호출 API KEY : 정렬필요 없어서 변수로 체크해서 처리
    > 상위 3개의 API의 ServiceID와 각각의 요청 수 : 상위 3의 API(e3ea, 2jdc, 23jf)에 해당하는 데이터 건수 추출위해 List에서 URL을 parsing 공통 메소드 호출해  카운트 재 추출 
       Map value 정렬이므로 List 생성. List 내림차순 정렬 후 상위 3건만 나오게 처리
    > 데이터 정합 체크 (일치해야 정상)
         e3ea 493
         2jdc  488
         23jf  483
         ---------
	          1464
	 
         blog: 260
        news: 246
        image: 245
     knowledge: 244
          vclip: 235
         book: 234
        -------------
	          1464

   > 웹브라우저별 사용 비율 : 퍼센트가 높은 건 순 정렬 필요해 Map value로 List 생성. List 내림차순 정렬 처리   

 5. 해당 구간마다 StringBuffer로 추가해 최종 내용 output.log 파일에 쓰기 처리

~~~

---


##결과출력-output.log

~~~

최다호출 API KEY
e3ea

상위 3개의 API ServiceID와 각각의 요청 수
blog: 260
news: 246
image: 245

웹브라우저별 사용 비율
IE: 85%
Firefox: 6%
Opera: 3%
Chrome: 2%
Safari: 2%

~~~